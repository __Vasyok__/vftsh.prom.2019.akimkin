﻿#include <iostream>

struct TDequeEl {
    int Data;
    TDequeEl* Next = nullptr;
    TDequeEl* Prev = nullptr;
};

class TDeque {
    unsigned int Len;
    TDequeEl* FrontHead = nullptr;
    TDequeEl* BackHead = nullptr;
public:
    TDeque() : Len(0) {}

    void PopFront() {
        if (FrontHead) {
            TDequeEl * newEl = new TDequeEl;
            newEl->Next = FrontHead->Next;
            delete FrontHead;
            FrontHead = newEl->Next;
            if (FrontHead) FrontHead->Prev = nullptr;
            if (!FrontHead) BackHead = newEl->Next;
            delete newEl;
            --Len;
        }
    }

    void PopBack() {
        if (BackHead) {
            TDequeEl * newEl = new TDequeEl;
            newEl->Prev = BackHead->Prev;
            delete BackHead;
            BackHead = newEl->Prev;
            if (BackHead) BackHead->Next = nullptr;
            if (!BackHead) FrontHead = nullptr;
            delete newEl;
            --Len;
        }
    }

    int TopFront() {
        if (FrontHead) return FrontHead->Data; return 0;
    }

    int TopBack() {
        if (BackHead != nullptr) return BackHead->Data; return 0;
    }

    void PushFront(int x) {
        TDequeEl* newEl = new TDequeEl;
        newEl->Data = x;
        newEl->Next = FrontHead;
        if (FrontHead) FrontHead->Prev = newEl;
        FrontHead = newEl;
        if (!BackHead) BackHead = newEl;
        ++Len;
    }
    void PushBack(int x) {
        TDequeEl* newEl = new TDequeEl;
        newEl->Data = x;
        newEl->Prev = BackHead;
        if (BackHead) BackHead->Next = newEl;
        BackHead = newEl;
        if (!FrontHead) FrontHead = newEl;
        ++Len;
    }
   void clean() {
       while (FrontHead) PopFront();
    }

    unsigned int Size() {
        return Len;
    }
};

int main() {}