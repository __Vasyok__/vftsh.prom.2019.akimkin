﻿
#include <iostream>
#include <vector>
#include <queue>

using std::queue;
using std::vector;

class TGraph{
	vector<vector<int>> list;
public:
	void dfs(int vertex) {
		vector<int> vertexes;
		vector<int> vertexesColour(list.size(), 0);
		vertexes.push_back(vertex);
		vertexesColour[vertex] = 1;
		while (!vertexes.empty()) {
			int x = vertexes.back();
			vertexes.pop_back();
			for (int i = 0; i < list[x].size(); ++i) {
				if (vertexesColour[list[x][i]] == 0) {
					vertexes.push_back(list[x][i]);
					vertexesColour[list[x][i]] = 1;
				}
			}
			vertexesColour[x] = 2;
		}
	}

	void bfs(int vertex) {
		queue<int> vertexes;
		vector<int> vertexesColour(list.size(), 0);
		vector<int> distance(list.size()), parent(list.size());
		vertexes.push(vertex);
		distance[vertex] = 0;
		parent[vertex] = -1;
		vertexesColour[vertex] = 1;
		while (!vertexes.empty()) {
			int x = vertexes.front();
			vertexes.pop();
			for (int i = 0; i < list[x].size(); ++i) {
				if (vertexesColour[list[x][i]] == 0) {
					vertexes.push(list[x][i]);
					distance[list[x][i]] = distance[x] + 1;
					parent[list[x][i]] = x;
					vertexesColour[list[x][i]] = 1;
				}
			}
			vertexesColour[x] = 2;
		}
	}

	//проверка на связность для неориентированных графов
	bool check_for_connectivity() {
		vector<int> vertexes;
		vector<int> vertexesColour(list.size(), 0);
		int count_of_passed_vertexes = 0;
		vertexes.push_back(0);
		vertexesColour[0] = 1;
		while (!vertexes.empty()) {
			int x = vertexes.back();
			vertexes.pop_back();
			for (int i = 0; i < list[x].size(); ++i) {
				if (vertexesColour[list[x][i]] == 0) {
					vertexes.push_back(list[x][i]);
					vertexesColour[list[x][i]] = 1;
				}
			}
			vertexesColour[x] = 2;
			++count_of_passed_vertexes;
		}
		if (count_of_passed_vertexes < list.size()) return false;
		else return true;
	}
	
	//подсчёт кличества компонент связности для неориентированных графов
	int count_of_connectivity_components() {
		vector<int> vertexes;
		vector<int> vertexesColour(list.size(), 0);
		int comps = 0;
		for (int i = 0; i < list.size(); ++i)
			if (vertexesColour[i] == 0) {
				vertexes.push_back(i);
				vertexesColour[i] = 1;
				while (!vertexes.empty()) {
					int x = vertexes.back();
					vertexes.pop_back();
					for (int j = 0; j < list[x].size(); ++j) {
						if (vertexesColour[list[x][j]] == 0) {
							vertexes.push_back(list[x][j]);
							vertexesColour[list[x][j]] = 1;
						}
					}
					vertexesColour[x] = 2;
				}
				++comps;
			}
		return comps;
	}

	//проверка на цикличность для неориентированных графов
	bool check_for_cycle() {
		vector<int> vertexes;
		vector<int> vertexesColour(list.size(), 0);
		vector<int> parent;
		for (int i = 0; i < list.size(); ++i) {
			if (vertexesColour[i] == 0) {
				vertexes.push_back(i);
				vertexesColour[i] = 1;
				while (!vertexes.empty()) {
					int x = vertexes.back();
					vertexes.pop_back();
					for (int j = 0; j < list[x].size(); ++j) {
						if (vertexesColour[list[x][j]] == 0) {
							vertexes.push_back(list[x][j]);
							vertexesColour[list[x][j]] = 1;
							parent[list[x][j]] = x;
						}
						else if (vertexesColour[list[x][j]] == 1) return true;
						else if (vertexesColour[list[x][j]] == 2 and parent[x] != list[x][j]) return true;
					}
					vertexesColour[x] = 2;
				}
			}
		}
		return false;
	}

	//проверка на дерево для неориентированных деревьев
	bool check_for_tree() {
		if (check_for_cycle == false and check_for_connectivity == true) return true;
		else return false;
	}
};
	